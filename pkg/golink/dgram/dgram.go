package dgram

import (
	"bytes"
	"encoding/binary"
	"fmt"

	"gitlab.com/basking2/golink/pkg/golink/msg"
)

type PROTOCOL byte

// Datagram header
type DatagramHeader struct {
	Version  byte
	Protocol PROTOCOL
	Length   uint16
	Crc      uint16
}

type Datagram struct {
	Header DatagramHeader
	Data   []byte
}

type DatagramServer struct {
	Msg             msg.Msg
	ProtocolBuffers map[PROTOCOL]*chan *Datagram
}

func NewDatagramServer(iface string) (*DatagramServer, error) {
	msg, err := msg.NewMsg(iface, msg.DEFAULT_ETHER_TYPE)
	if err != nil {
		return nil, err
	}

	return &DatagramServer{msg, make(map[PROTOCOL]*chan *Datagram)}, nil
}

func (server *DatagramServer) SendBytes(mac string, protocol PROTOCOL, data []byte) error {
	if len(data) > msg.MTU {
		return fmt.Errorf("message of length %d exceed MTU of %d", len(data), msg.MTU)
	}

	header := DatagramHeader{0x1, protocol, uint16(len(data)), 0x0000}

	body := make([]byte, 0, int(header.Length)+binary.Size(header))

	buffer := bytes.NewBuffer(body)

	// Write the header and body, but the CRC = 0.
	header.Crc = 0
	binary.Write(buffer, binary.BigEndian, header)
	binary.Write(buffer, binary.BigEndian, data)

	// Compute the CRC and re-write the header.
	header.Crc = computeCrc(0, buffer.Bytes())
	buffer = bytes.NewBuffer(nil)
	binary.Write(buffer, binary.BigEndian, header)
	binary.Write(buffer, binary.BigEndian, data)

	return server.Msg.SendBytes(mac, buffer.Bytes())
}

func (server *DatagramServer) RecvBytes() (*Datagram, error) {
	data := make([]byte, msg.MTU)
	recvlen, err := server.Msg.Recv(data)
	if err != nil {
		return nil, err
	}

	header := DatagramHeader{}

	if recvlen < 6 {
		return nil, fmt.Errorf("message of length %d is not long enough", binary.Size(header))
	}

	buffer := bytes.NewBuffer(data[0:binary.Size(header)])
	err = binary.Read(buffer, binary.BigEndian, &header)
	if err != nil {
		return nil, err
	}

	if header.Length > msg.MTU {
		return nil, fmt.Errorf("message length of %d is larger than MTU %d", header.Length, msg.MTU)
	}

	crc := computeCrc(header.Crc, data[0:header.Length+uint16(binary.Size(header))])
	if crc != header.Crc {
		return nil, fmt.Errorf("computed crc %d does not match message crc %d", crc, header.Crc)
	}

	return &Datagram{header, data[binary.Size(header) : binary.Size(header)+int(header.Length)]}, nil
}

func computeCrc(crc uint16, data []byte) uint16 {
	for i := 0; i < len(data)-1; i += 2 {
		var v uint16 = uint16(data[i])
		v = (v<<8 | uint16(data[i+1]))
		crc = crc ^ v
	}

	// Odd crc. Get last byte and add it.
	if len(data)%2 == 1 {
		var v uint16 = uint16(data[len(data)-1]) << 8
		crc = crc ^ v
	}

	return crc
}

func (server *DatagramServer) Close() {
	server.Msg.Destroy()
}
