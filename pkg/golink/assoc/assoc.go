package assoc

import (
	"fmt"

	"gitlab.com/basking2/golink/pkg/golink/msg"
)

// How user code handles call-backs from the GoLinkAssoc running
// in the go routine that handles the state of the GoLinkAssoc.
type GoLinkAssocHandler interface {
	Recv([]byte) error
	Close() error
}

// A GoLink association. This is the sending of data to an from a single
// MAC with a particular ethertype.
type GoLinkAssoc struct {
	// The string name of the interface to use.
	iface string

	// Destination mac address.
	mac string

	// The ethernet protocol to use.
	protocol uint16

	started bool

	handler GoLinkAssocHandler

	sendChannel chan []byte
	recvChannel chan []byte

	// Channel that delivers functions to be processed in the
	// send/recv go routine. This gives a form of exclusive access related
	// to processing data from the association.
	//
	// These functions will block processing of send/recv messages while running.
	funcChannel chan func(*GoLinkAssoc)

	msg msg.Msg
}

// Create a new *GoLinkAssoc. Created associations must be started with Start().
//
// protocol is the ethernet type that will be used.
// iface is the name of the interface to use, eg eth0.
// mac is the mac address to send to.
// handler this is how the user interacts with this object.
func NewGoLinkAssoc(
	protocol uint16,
	iface string,
	mac string,
	handler GoLinkAssocHandler) *GoLinkAssoc {
	gla := GoLinkAssoc{}

	gla.handler = handler
	gla.iface = iface
	gla.mac = mac
	gla.protocol = protocol

	return &gla
}

func (assoc *GoLinkAssoc) Start() error {

	if assoc.started {
		return fmt.Errorf("already started")
	}

	if msg, err := msg.NewMsg(assoc.iface, assoc.protocol); err != nil {
		return err
	} else {
		assoc.msg = msg
	}

	buffer := 10
	assoc.sendChannel = make(chan []byte, buffer)
	assoc.recvChannel = make(chan []byte, buffer)
	assoc.funcChannel = make(chan func(*GoLinkAssoc), buffer)

	// Go routine that receives data.
	go func(m *msg.Msg, recv chan []byte) {
		for {
			buffer := make([]byte, msg.MTU)
			if len, err := m.Recv(buffer); err != nil {
				// Error out.
				close(recv)
				return
			} else {
				recv <- buffer[0:len]
			}
		}
	}(&assoc.msg, assoc.recvChannel)

	// This goroutine will process received bytes (via the handler)
	// and transmit package from the user.
	go func(mac string, m *msg.Msg, sendChannel, recvChannel chan []byte, funcChannel chan func(*GoLinkAssoc)) {
		for {

			// Poll the send channel first.
			// We do this because Go does not guarantee to poll sendChannel first.
			// This means that the funcChan may be called many times and fill the
			// send buffer and block, effectively dead-locking the system.
			select {
			case b := <-sendChannel:
				m.SendBytes(mac, b)
			default:
				// Nop!
			}

			select {
			case b := <-sendChannel:
				m.SendBytes(mac, b)
			case b := <-recvChannel:
				assoc.handler.Recv(b)
			case f := <-funcChannel:
				f(assoc)
			}

			// log.Printf("send is %d, recv is %d, func is %d", len(sendChannel), len(recvChannel), len(funcChannel))
		}
	}(assoc.mac, &assoc.msg, assoc.sendChannel, assoc.recvChannel, assoc.funcChannel)

	assoc.started = true

	return nil
}

func (assoc *GoLinkAssoc) Send(data []byte) error {
	// FIXME - future work, on full channel, block or return try-again error?
	assoc.sendChannel <- data
	return nil
}

func (assoc *GoLinkAssoc) Stop() error {
	if assoc.started {
		// Destroy() will cause a receive error and stop the receive go routine.
		assoc.msg.Destroy()

		// This will (and the recvChannel getting closed) will cause the second
		// goroutine to exit.
		close(assoc.sendChannel)

		// Null the msg.
		assoc.msg = msg.Msg{}
		return assoc.handler.Close()
	}

	return nil
}

// Schedule the given function to execute in the goroutine managing this
// association.
//
// This provides a form of mutual exclusion to mutate some state that
// informs the routing of received and sent messages.
func (assoc *GoLinkAssoc) DoInAssoc(f func(*GoLinkAssoc)) error {
	assoc.funcChannel <- f
	return nil
}
