package assoc

type GoLinkAssocReaderWriter struct {
	assoc       *GoLinkAssoc
	reader      chan []byte
	unreadBytes []byte
}

func (reader *GoLinkAssocReaderWriter) Recv(msg []byte) error {
	reader.reader <- msg
	return nil
}

func (reader *GoLinkAssocReaderWriter) Close() error {
	reader.assoc.Stop()
	close(reader.reader)
	return nil
}

func (writer *GoLinkAssocReaderWriter) Write(data []byte) (int, error) {
	return writer.assoc.Write(data)
}

func (reader *GoLinkAssocReaderWriter) Read(p []byte) (n int, err error) {
	if reader.unreadBytes != nil {
		read_bytes := copy(p, reader.unreadBytes)

		if read_bytes < len(reader.unreadBytes) {
			reader.unreadBytes = reader.unreadBytes[read_bytes:]
		} else {
			reader.unreadBytes = nil
		}
	}

	msg := <-reader.reader
	if msg == nil {
		return 0, nil
	}

	read_bytes := copy(p, msg)
	if read_bytes < len(msg) {
		reader.unreadBytes = msg[read_bytes:]
	}

	return read_bytes, nil
}

func NewGoLinkAssocReaderWriter(
	protocol uint16,
	iface string,
	mac string) *GoLinkAssocReaderWriter {
	rw := GoLinkAssocReaderWriter{}
	gla := NewGoLinkAssoc(protocol, iface, mac, &rw)

	rw.reader = make(chan []byte, 1024)
	rw.assoc = gla

	gla.Start()

	return &rw
}
