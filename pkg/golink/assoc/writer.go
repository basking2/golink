package assoc

import (
	"fmt"

	"gitlab.com/basking2/golink/pkg/golink/msg"
)

func (assoc *GoLinkAssoc) Write(data []byte) (int, error) {

	if len(data) > msg.MTU {
		return 0, fmt.Errorf("data length %d must be less than or equal to the MUT %d", len(data), msg.MTU)
	}

	// FIXME - future work, on full channel, block or return try-again error?
	assoc.sendChannel <- data

	return len(data), nil
}
