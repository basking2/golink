package connection

import (
	"fmt"

	"gitlab.com/basking2/golink/pkg/golink/assoc"
	"gitlab.com/basking2/golink/pkg/golink/msg"
	"gitlab.com/basking2/golink/pkg/sdsai/bytestream"
	"gitlab.com/basking2/golink/pkg/sdsai/bytewindow"
)

type PORT uint16
type SEQUENCE_NUMBER uint32

// The connection protocol. This is stream or dgram.
type PROTOCOL uint8

var (
	// Reference of TCP, but this is not TCP.
	STREAM = PROTOCOL(0x6)

	// Reference to UDP, but this is not UDP.
	DGRAM = PROTOCOL(0x11)
)

type Connector struct {
	assoc     *assoc.GoLinkAssoc
	streaming map[ports]*StreamConnection
	dgram     map[ports]*DgramConnection
}

type ports struct {
	local  PORT
	remote PORT
}

func (c *Connector) Close() error {
	return c.assoc.Stop()
}

func get_protocol(msg []byte) (PROTOCOL, error) {
	if len(msg) < 1 {
		return 0, fmt.Errorf("msg too short to get protocol")
	}

	return PROTOCOL(msg[0]), nil
}

// Called in Association's go routine.
func (c *Connector) Recv(msg []byte) error {

	if IsDgram(msg) {
		hdr := DgramHeader{}
		if hlen, err := hdr.DecodeHeader(msg); err != nil {
			return err
		} else if conn, ok := c.dgram[ports{local: hdr.DestinationPort, remote: hdr.SourcePort}]; !ok {
			return fmt.Errorf("no such connection for local port %d and remote port %d", hdr.DestinationPort, hdr.SourcePort)
		} else if err := conn.write(msg[hlen:]); err != nil {
			return err
		}

	} else if IsStreaming(msg) {
		hdr := StreamHeader{}
		if hlen, err := hdr.DecodeHeader(msg); err != nil {
			return err
		} else if conn, ok := c.streaming[ports{local: hdr.DestinationPort, remote: hdr.SourcePort}]; !ok {
			return fmt.Errorf("no such connection for local port %d and remote port %d", hdr.DestinationPort, hdr.SourcePort)
		} else if err := conn.write(hdr, msg[hlen:]); err != nil {
			return err
		}
	} else if len(msg) < 1 {
		return fmt.Errorf("msg is too short to contain anything")
	} else {
		return fmt.Errorf("unsupported protocol %d", msg[0])
	}

	return nil
}

func NewConnector(protocol uint16, iface, mac string) (*Connector, error) {

	connector := Connector{
		streaming: make(map[ports]*StreamConnection),
		dgram:     make(map[ports]*DgramConnection),
	}

	a := assoc.NewGoLinkAssoc(protocol, iface, mac, &connector)

	connector.assoc = a

	e := a.Start()
	if e != nil {
		return nil, e
	}

	return &connector, nil
}

func (c *Connector) NewConnection(protocol PROTOCOL, SrcPort, DstPort PORT) Connection {
	ch := make(chan Connection)

	defer close(ch)

	c.assoc.DoInAssoc(func(gla *assoc.GoLinkAssoc) {
		switch protocol {
		case STREAM:
			st := StreamConnection{}
			st.dstPort = DstPort
			st.srcPort = SrcPort
			st.connector = c
			st.msgIn = make(chan []byte)
			var err error
			st.outputStream, err = bytestream.NewByteStream(100000, msg.MTU)
			if err != nil {
				panic(err)
			}

			st.inputStream, err = bytewindow.NewByteWindow(100000, 0)
			if err != nil {
				panic(err)
			}

			st.retransmit = nil
			c.streaming[ports{local: SrcPort, remote: DstPort}] = &st

			ch <- &st
		case DGRAM:
			dg := DgramConnection{}
			dg.dstPort = DstPort
			dg.srcPort = SrcPort
			dg.connector = c
			dg.msgIn = make(chan []byte)
			c.dgram[ports{local: SrcPort, remote: DstPort}] = &dg
			ch <- &dg
		default:
			panic(fmt.Errorf("unsupported connection protocol"))
		}

	})

	return <-ch
}
