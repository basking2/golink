package connection

// The retry algorithm is somewhat simple.
// When a byte is sent, a timer is set.
// No new timer is set. When the timer expires,
// all un-acked segments are retransmitted and a new timer is set.

import (
	"fmt"
	"log"
	"time"

	"gitlab.com/basking2/golink/pkg/golink/assoc"
	"gitlab.com/basking2/golink/pkg/sdsai/bytestream"
	"gitlab.com/basking2/golink/pkg/sdsai/retry"
)

var RETRY_BACKOFF, _ = time.ParseDuration("500ms")
var RETRY_BACKOFF_MAX, _ = time.ParseDuration("4s")
var RETRY_COUNT = 6

func (c *StreamConnection) retry_impl() error {

	// If in here, we are retrying the send.
	current_seqnum := c.outputStream.SequenceNumber

	first_seg_unacked := false
	some_seg_unacked := false

	// Examine all outstanding byte segments.
	c.outputStream.WalkSegments(func(bs *bytestream.ByteStream, bss *bytestream.ByteStreamSegment) bool {

		if !bss.Acknowledged {

			first_seg_unacked = current_seqnum == c.outputStream.SequenceNumber
			some_seg_unacked = true

			// Re-transmit the byte segment. This should include any header information.
			c.connector.assoc.DoInAssoc(func(assoc *assoc.GoLinkAssoc) {
				if _, err := assoc.Write(bss.Data); err != nil {
					log.Default().Printf("Error sending %s\n", err)
				}
			})
		}

		current_seqnum += bss.Size

		// Check all segments!
		return true
	})

	if first_seg_unacked {
		return fmt.Errorf("first segment unacked, continue retry algorithm")
	} else if some_seg_unacked {
		c.retransmit.Retry(c.retry_impl)
	} else {
		c.connector.assoc.DoInAssoc(func(assoc *assoc.GoLinkAssoc) {
			c.retransmit.Stop()
			c.retransmit = nil
		})
	}

	return nil
}

// Start the re-transmit algorithm.
// This must be called in the DoInAssoc handler to ensure mutual exclusion to
// the retry timer.
func (c *StreamConnection) retx(called_from_do_in_assoc bool) error {

	if !called_from_do_in_assoc {
		panic("retx was not called from the DoInAssoc handler")
	}

	if c.retransmit != nil {
		log.Default().Printf("retransmit timer already scheduled")
		return nil
	}

	rt, err := retry.NewExponentialBackoffRetry(RETRY_COUNT, RETRY_BACKOFF, RETRY_BACKOFF_MAX)

	if err != nil {
		return err
	}

	c.retransmit = rt

	rt.Retry(c.retry_impl)

	return nil
}
