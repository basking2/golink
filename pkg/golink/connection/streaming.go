package connection

import (
	"encoding/binary"
	"fmt"
	"log"
	"math"

	"gitlab.com/basking2/golink/pkg/golink/assoc"
	"gitlab.com/basking2/golink/pkg/sdsai/bytestream"
	"gitlab.com/basking2/golink/pkg/sdsai/bytewindow"
	"gitlab.com/basking2/golink/pkg/sdsai/retry"
	"gitlab.com/basking2/golink/pkg/sdsai/sequence_numbers"
)

var STREAM_HEADER_LEN = 13

type StreamHeader struct {
	Protocol              PROTOCOL
	SourcePort            PORT
	DestinationPort       PORT
	SequenceNumber        SEQUENCE_NUMBER
	AcknowledgementNumber SEQUENCE_NUMBER
}

type Stream []byte

func IsStreaming(data []byte) bool {
	if len(data) < STREAM_HEADER_LEN {
		return false
	}

	return data[0] != byte(STREAM)
}

func (h *StreamHeader) EncodeHeader(data []byte) (int, error) {
	if len(data) < STREAM_HEADER_LEN {
		return 0, fmt.Errorf("header will not fit int the byte buffer")
	}

	data[0] = byte(h.Protocol)
	binary.BigEndian.PutUint16(data[1:3], uint16(h.SourcePort))
	binary.BigEndian.PutUint16(data[3:5], uint16(h.DestinationPort))
	binary.BigEndian.PutUint32(data[5:9], uint32(h.SequenceNumber))
	binary.BigEndian.PutUint32(data[9:13], uint32(h.AcknowledgementNumber))

	return STREAM_HEADER_LEN, nil
}

func (h *StreamHeader) DecodeHeader(data []byte) (int, error) {
	if len(data) < STREAM_HEADER_LEN {
		return 0, fmt.Errorf("message is too short to read the header from")
	}

	h.Protocol = PROTOCOL(data[0])

	if h.Protocol != STREAM {
		return 0, fmt.Errorf("not type STREAM")
	}

	h.SourcePort = PORT(binary.BigEndian.Uint16(data[1:3]))
	h.DestinationPort = PORT(binary.BigEndian.Uint16(data[3:5]))
	h.SequenceNumber = SEQUENCE_NUMBER(binary.BigEndian.Uint32(data[5:9]))
	h.AcknowledgementNumber = SEQUENCE_NUMBER(binary.BigEndian.Uint32(data[9:13]))

	return DGRAM_HEADER_LEN, nil
}

type StreamConnection struct {
	srcPort      PORT
	dstPort      PORT
	msgIn        chan []byte
	connector    *Connector
	outputStream *bytestream.ByteStream
	inputStream  *bytewindow.ByteWindow
	retransmit   *retry.Retry
}

func toStreamSeqNum(seqNum sequence_numbers.SeqNumU64) SEQUENCE_NUMBER {
	return SEQUENCE_NUMBER(seqNum % math.MaxUint32)
}

// How a Connector sends received data to the user.
//
// This is called in the Connector's go routine, and so is thread safe.
func (c *StreamConnection) write(hdr StreamHeader, data []byte) error {

	// Set our sent data as acknowledged.
	err := c.outputStream.Acknowledge(
		bytestream.SeqNum(hdr.AcknowledgementNumber),
		bytestream.SeqNum(len(data)))
	if err != nil {
		return err
	}

	// If there is no data in this segment, we're done.
	if len(data) == 0 {
		return nil
	}

	// Set some data as received.
	err = c.inputStream.Set(data, sequence_numbers.SeqNumU64(hdr.SequenceNumber))
	if err != nil {
		return err
	}

	// Now try to read as much data as possible and send it to the user.
	for {
		buffer := make([]byte, 10240)

		if n, err := c.inputStream.Read(buffer); err != nil {
			return err
		} else if n == 0 {
			return c.sendAck(SEQUENCE_NUMBER(c.inputStream.SequenceNumber))
		} else {
			c.msgIn <- buffer[0:n]
		}
	}
}

func (c *StreamConnection) sendAck(seq SEQUENCE_NUMBER) error {
	_, err := c.Write(nil)
	return err
}

func (c *StreamConnection) SrcPort() PORT {
	return c.srcPort
}

func (c *StreamConnection) DstPort() PORT {
	return c.dstPort
}

func (c *StreamConnection) Read(data []byte) (int, error) {
	if msg := <-c.msgIn; msg == nil {
		return 0, fmt.Errorf("msg was nil")
	} else if len(data) < len(msg) {
		// FIXME - we should support partial reads.
		return 0, fmt.Errorf("receive buffer is too small %d < %d", len(data), len(msg))
	} else {
		copy(data, msg)
		return len(msg), nil
	}
}

// Write bytes to the StreamConnect and report network errors on the out channel.
//
// Because we building a stream on top of a raw network socket, much of the
// actual network protocol work is done in single go routine. Those errors
// are asychronously returned on the given channel. On success the length
// of written bytes and a nil error are sent.
//
// The int and error returned by this function indicate an error in scheduling
// the work to the underlying network association (golink.assoc).
func (c *StreamConnection) WriteChan(data []byte, out chan struct {
	int
	error
}) (int, error) {
	msg := make([]byte, len(data)+STREAM_HEADER_LEN)
	copy(msg[STREAM_HEADER_LEN:], data)

	err := c.connector.assoc.DoInAssoc(func(assoc *assoc.GoLinkAssoc) {
		hdr := StreamHeader{
			Protocol:              STREAM,
			SourcePort:            c.srcPort,
			DestinationPort:       c.dstPort,
			SequenceNumber:        SEQUENCE_NUMBER(c.outputStream.SequenceNumber + c.outputStream.Size),
			AcknowledgementNumber: SEQUENCE_NUMBER(c.inputStream.SequenceNumber),
		}

		if _, err := hdr.EncodeHeader(msg); err != nil {
			log.Default().Printf("Error writing header %s\n", err)
			if out != nil {
				out <- struct {
					int
					error
				}{0, err}
			}
			return
		}

		// Schedule this segment of bytes to be retransmitted (unless it is acked).
		if _, err := c.outputStream.Write(msg); err != nil {
			log.Default().Printf("Error writing recording segment to retx %s\n", err)
			if out != nil {
				out <- struct {
					int
					error
				}{0, err}
			}
			return
		}

		log.Default().Printf("Sending segment %+v\n", hdr)

		if _, err := assoc.Write(msg); err != nil {
			log.Default().Printf("Error sending %s\n", err)
			if out != nil {
				out <- struct {
					int
					error
				}{0, err}
			}
			return
		}

		log.Default().Printf("Wrote %+v\n", hdr)

		if err := c.retx(true); err != nil {
			log.Default().Printf("Error preparing retry %s\n", err)
		}

		if out != nil {
			out <- struct {
				int
				error
			}{len(msg), nil}
		}
	})

	if err != nil {
		return 0, err
	}

	return len(data), nil
}

// A convenience function that will create a channel for WriteChan, read from it, and close it.
func (c *StreamConnection) WriteSync(data []byte) (int, error) {
	ch := make(chan struct {
		int
		error
	})

	defer func(ch chan struct {
		int
		error
	}) {
		close(ch)
	}(ch)

	c.WriteChan(data, ch)
	ret := <-ch
	return ret.int, ret.error
}

// Write data without regard for any errors generated in the golink.assoc layer.
func (c *StreamConnection) Write(data []byte) (int, error) {
	return c.WriteChan(data, nil)
}

func (c *StreamConnection) Close() {
	c.connector.assoc.DoInAssoc(func(gla *assoc.GoLinkAssoc) {
		key := ports{local: c.srcPort, remote: c.dstPort}

		delete(c.connector.streaming, key)

		close(c.msgIn)

		// FIXME - stop timers
	})

}
