package connection

import (
	"encoding/binary"
	"fmt"

	"gitlab.com/basking2/golink/pkg/golink/assoc"
)

var DGRAM_HEADER_LEN = 5

type Dgram []byte

type DgramHeader struct {
	Protocol        PROTOCOL
	SourcePort      PORT
	DestinationPort PORT
}

func IsDgram(data []byte) bool {
	if len(data) < DGRAM_HEADER_LEN {
		return false
	}

	return data[0] != byte(DGRAM)
}

func (h *DgramHeader) EncodeHeader(data []byte) (int, error) {
	if len(data) < DGRAM_HEADER_LEN {
		return 0, fmt.Errorf("header will not fit int the byte buffer")
	}

	data[0] = byte(h.Protocol)
	binary.BigEndian.PutUint16(data[1:3], uint16(h.SourcePort))
	binary.BigEndian.PutUint16(data[3:5], uint16(h.DestinationPort))

	return DGRAM_HEADER_LEN, nil
}

func (h *DgramHeader) DecodeHeader(data []byte) (int, error) {
	if len(data) < DGRAM_HEADER_LEN {
		return 0, fmt.Errorf("message is too short to read the header from")
	}

	h.Protocol = PROTOCOL(data[0])

	if h.Protocol != DGRAM {
		return 0, fmt.Errorf("not type DGRAM")
	}

	h.SourcePort = PORT(binary.BigEndian.Uint16(data[1:3]))
	h.DestinationPort = PORT(binary.BigEndian.Uint16(data[3:5]))

	return DGRAM_HEADER_LEN, nil
}

type DgramConnection struct {
	srcPort   PORT
	dstPort   PORT
	msgIn     chan []byte
	connector *Connector
}

func (c *DgramConnection) write(data []byte) error {
	c.msgIn <- data
	return nil
}

func (c *DgramConnection) SrcPort() PORT {
	return c.srcPort
}

func (c *DgramConnection) DstPort() PORT {
	return c.dstPort
}

func (c *DgramConnection) Read(data []byte) (int, error) {
	if msg := <-c.msgIn; msg == nil {
		return 0, fmt.Errorf("msg was nil")
	} else if cap(data) < len(msg) {
		return 0, fmt.Errorf("receive buffer is too small %d < %d", cap(data), len(msg))
	} else {
		copy(data, msg)
		return len(msg), nil
	}
}

func (c *DgramConnection) WriteChan(data []byte, out chan struct {
	int
	error
}) (int, error) {
	msg := make([]byte, len(data)+DGRAM_HEADER_LEN)

	hdr := DgramHeader{Protocol: DGRAM, SourcePort: c.srcPort, DestinationPort: c.dstPort}

	if _, err := hdr.EncodeHeader(msg); err != nil {
		return 0, err
	}

	copy(msg[DGRAM_HEADER_LEN:], data)

	// FIXME FIXME -- capture errrs in assoc goroutine.
	return c.connector.assoc.Write(msg)
}

func (c *DgramConnection) WriteSync(data []byte) (int, error) {
	ch := make(chan struct {
		int
		error
	})

	defer func(ch chan struct {
		int
		error
	}) {
		close(ch)
	}(ch)

	c.WriteChan(data, ch)
	ret := <-ch
	return ret.int, ret.error
}

func (c *DgramConnection) Write(data []byte) (int, error) {
	msg := make([]byte, len(data)+DGRAM_HEADER_LEN)

	hdr := DgramHeader{Protocol: DGRAM, SourcePort: c.srcPort, DestinationPort: c.dstPort}

	if _, err := hdr.EncodeHeader(msg); err != nil {
		return 0, err
	}

	copy(msg[DGRAM_HEADER_LEN:], data)

	return c.connector.assoc.Write(msg)
}

func (c *DgramConnection) Close() {
	c.connector.assoc.DoInAssoc(func(gla *assoc.GoLinkAssoc) {
		key := ports{local: c.srcPort, remote: c.dstPort}

		delete(c.connector.dgram, key)

		close(c.msgIn)
	})
}
