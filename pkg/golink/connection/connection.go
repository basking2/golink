package connection

import "io"

type Connection interface {
	SrcPort() PORT
	DstPort() PORT

	Close()

	io.Reader
	io.Writer

	WriteSync(data []byte) (int, error)
	WriteChan(data []byte, out chan struct {
		int
		error
	}) (int, error)
}
