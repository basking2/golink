#if defined(__linux__) && !defined(__ANDROID__)
#include "msg.h"

#include <sys/errno.h>
#include <sys/socket.h>
#include <sys/types.h>

#include <stdio.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <net/ethernet.h>
#include <ifaddrs.h>
#include <string.h>
#include <linux/if_packet.h>
#include <sys/ioctl.h>
#include <net/if.h>

struct msg {
    int socket;
    int ether_type;
    const char* error_msg;
    struct sockaddr_ll addr;
};

const char *error_msg(msg_t m) {
    return m->error_msg;
}

int ifindex(int socket, const char *iface_name) {
    struct ifreq r;
    if (strlen(iface_name) >= IFNAMSIZ) {
        return -1;
    }

    strcpy(r.ifr_name, iface_name);
    int rc = ioctl(socket, SIOCGIFINDEX, &r);
    if (rc == -1) {
        return rc;
    }

    return r.ifr_ifindex;
}

/**
 * Return in the name parameter the name of interface listed at the given index.
 * The name parameter must be at least IFNAMSIZ in length.
 * If the index is not found, -1 is returned, 0 otherwise.
 */
int ifname(int socket, int index, char *name) {
    struct ifreq r;
    r.ifr_ifindex = index;
    int rc = ioctl(socket, SIOCGIFNAME, &r);
    if (rc == -1) {
        return rc;
    }

    strcpy(name, r.ifr_name);
    return 0;
}

int setaddr(unsigned char* addr, const char* addrstr) {
    unsigned int v[6];
    int rc = sscanf(addrstr, "%02x:%02x:%02x:%02x:%02x:%02x", &v[0], &v[1], &v[2], &v[3], &v[4], &v[5]);

    addr[0] = v[0];
    addr[1] = v[1];
    addr[2] = v[2];
    addr[3] = v[3];
    addr[4] = v[4];
    addr[5] = v[5];

    if (rc != 6) {
        return -1;
    }
    return rc;
}

msg_t new_msg(const char* iface_name, const int ether_type) {
    int rc;

    msg_t m = new struct msg();
    m->error_msg = NULL;
    m->ether_type = ether_type;
    
    m->socket = socket(AF_PACKET, SOCK_DGRAM, ether_type);
    if (m->socket == -1) {
        m->error_msg = strerror(errno);
        return m;
    }
    
    m->addr.sll_family = AF_PACKET;
    m->addr.sll_ifindex = ifindex(m->socket, iface_name);
    if (m->addr.sll_ifindex == -1) {
        m->error_msg = strerror(errno);
        return m;
    }
    m->addr.sll_hatype = 0;
    m->addr.sll_protocol = htons(ether_type);
    m->addr.sll_pkttype = 0;

    return m;

}

void send_msg(msg_t m, const char* dst, const char* data, size_t len) {
    m->error_msg = NULL;
    // printf("%s\n", data);
    // printf("%d %d\n", len, sizeof(m->addr));
    m->addr.sll_halen = setaddr(m->addr.sll_addr, dst);
    if (m->addr.sll_halen == -1) {
        m->error_msg = strerror(errno);
        return;
    }

    ssize_t sent = sendto(m->socket, data, len, 0, (sockaddr*)&(m->addr), sizeof(m->addr));
    if (sent == -1) {
        m->error_msg = strerror(errno);
    }
}

void destroy_msg(msg_t msg) {
    if (msg->socket >= 0){
        close(msg->socket);
    }
    delete msg;
}

ssize_t recv_msg(msg_t m, void* buffer, size_t buffer_sz) {
    return recvfrom(m->socket, buffer, buffer_sz, 0, NULL, NULL);
}

int socket_msg(msg_t m) {
    return m->socket;
}


#endif // defined(__linux__) && !defined(__ANDROID__)
