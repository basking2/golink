#if defined(__APPLE__)

#include "msg.h"
#include <sys/socket.h>
#include <net/if.h>
#include <net/ndrv.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <net/ethernet.h>
#include <errno.h>

// Example here: http://newosxbook.com/bonus/vol1ch16.html

struct msg {
    int socket;
    uint16_t ether_type;
    unsigned char mac[6];
    struct sockaddr_ndrv addr;
    const char* error_msg;
};

const char * error_msg(msg_t m) {
    return m->error_msg;
}

int setaddr(unsigned char* addr, const char* addrstr) {
    unsigned int v[6];
    int rc = sscanf(addrstr, "%02x:%02x:%02x:%02x:%02x:%02x", &v[0], &v[1], &v[2], &v[3], &v[4], &v[5]);

    addr[0] = v[0];
    addr[1] = v[1];
    addr[2] = v[2];
    addr[3] = v[3];
    addr[4] = v[4];
    addr[5] = v[5];

    if (rc != 6) {
        return -1;
    }
    return rc;
}

int setup_to_receive(msg_t m, const int ether_type) {
    struct ndrv_protocol_desc desc;
    struct ndrv_demux_desc demux_desc;
    memset(&desc, '\0', sizeof(desc));
    memset(&demux_desc, '\0', sizeof(demux_desc));

    /* Request kernel for demuxing of one chosen ethertype */
    desc.version = NDRV_PROTOCOL_DESC_VERS;
    desc.protocol_family = ether_type;
    desc.demux_count = 1;
    desc.demux_list = &demux_desc;
    demux_desc.type = NDRV_DEMUXTYPE_ETHERTYPE;
    demux_desc.length = sizeof(demux_desc.data.ether_type);
    demux_desc.data.ether_type = htons(ether_type);

    //if (setsockopt(m->socket, SOL_NDRVPROTO, NDRV_DELDMXSPEC, (caddr_t)&desc, sizeof(desc)) < 0) {
           //perror("Clearing.");
    //}

    return setsockopt(m->socket, 
        SOL_NDRVPROTO, 
        NDRV_SETDMXSPEC, 
        (caddr_t)&desc, sizeof(desc));
}

msg_t new_msg(const char* iface_name, const int ether_type){
    msg_t m = new struct msg();

    m->socket = socket(PF_NDRV,SOCK_RAW,0);
    if (m->socket == -1) {
        m->error_msg = strerror(errno);
        return m;
    }

    strlcpy((char *)m->addr.snd_name, iface_name, sizeof(m->addr.snd_name));
    m->addr.snd_family = PF_NDRV;
    m->addr.snd_len = sizeof(m->addr);
    m->ether_type = ether_type;

    int rc = bind(m->socket, (struct sockaddr *) &m->addr, sizeof(m->addr));
    if (rc < 0) {
        fprintf(stderr, "BOOM");
        m->error_msg = strerror(errno);
        return m;
    }

    rc = setup_to_receive(m, ether_type);
    if (rc < 0) {
        m->error_msg = strerror(errno);
        return m;
    }

    return m;
}

ssize_t recv_msg(msg_t m, void* buffer, size_t sz) {
    char data[sz+14];
    int rc = recv(m->socket, data, sz, 0);
    if (rc >=14) {
        rc -= 14;
        memcpy(buffer, data+14, rc);
    }

    return rc;
}

void send_msg(msg_t m, const char* dst, const char* data, size_t len) {
    m->error_msg = NULL;
    uint16_t etherType = htons(m->ether_type);
    char packetBuffer[14+len];

    int rc = setaddr(m->mac, dst);
    if (rc < 0) {
        m->error_msg = "Parse MAC";
        return;
    }


    // Set destination.
    memcpy(packetBuffer, m->mac, 6);

    // Set source.
    memset(packetBuffer+6, '\xff', 6);

    // Set ether type.
    packetBuffer[12] = (etherType >> 8) & 0xff;
    packetBuffer[13] = (etherType >> 0) & 0xff;

    // Set user data.
    memcpy(packetBuffer + 14, data, len);

    // Transmit.
    rc = sendto(m->socket, packetBuffer, 14+len, 0, (struct sockaddr *)&m->addr, sizeof(m->addr));
    if (rc == -1) {
        m->error_msg = strerror(errno);
    }
}

void destroy_msg(msg_t m) {
    if (m->socket >= 0){
        close(m->socket);
    }
    delete m;
}


#endif
