#ifndef __MSG_H

#define __MSG_H

#ifdef __cplusplus
extern "C" {
#endif

#include <sys/types.h>

struct msg;

typedef struct msg* msg_t;

/**
 * @param[in] iface_name Interface name to send from.
 * @param[in] dst The destination MAC to send to.
 * @param[in] ether_type The ether type.
 */
msg_t new_msg(const char* iface_name, const int ether_type);
void send_msg(msg_t, const char*, const char*, size_t);
ssize_t recv_msg(msg_t, void*, size_t);
int socket_msg(msg_t);
const char *error_msg(msg_t);
void destroy_msg(msg_t);

#ifdef __cplusplus
}
#endif

#endif // __MSG_H