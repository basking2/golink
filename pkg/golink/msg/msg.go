package msg

// #include <stdio.h>
// #include <stdlib.h>
// #include "msg.h"
import "C"
import (
	"errors"
	"fmt"
	"runtime"
	"unsafe"
)

type Msg struct {
	Id  string
	msg C.msg_t
}

const DEFAULT_ETHER_TYPE = uint16(0x88b5)

const MTU = 1000

func NewMsg(iface string, protocol uint16) (Msg, error) {
	iface_cstr := C.CString(iface)
	defer C.free(unsafe.Pointer(iface_cstr))
	m := C.new_msg(iface_cstr, C.int(protocol))
	if C.error_msg(m) != nil {
		err := errors.New(C.GoString(C.error_msg(m)))
		C.destroy_msg(m)
		return Msg{"", nil}, err
	}

	var id = fmt.Sprintf("%s:%d", iface, protocol)

	return Msg{id, m}, nil
}

func (m *Msg) Destroy() {
	if m.msg != nil {
		C.destroy_msg(m.msg)
	}
}

func (m *Msg) SendString(mac, msg string) error {
	if len(msg) > MTU {
		return fmt.Errorf("message of length %d is larger than MTU %d", len(msg), MTU)
	}

	mac_cstr := C.CString(mac)
	defer C.free(unsafe.Pointer(mac_cstr))

	s := C.CString(msg)
	defer C.free(unsafe.Pointer(s))
	C.send_msg(m.msg, mac_cstr, s, C.size_t(len(msg)))

	if C.error_msg(m.msg) != nil {
		return errors.New(C.GoString(C.error_msg(m.msg)))
	}

	return nil
}

func (m *Msg) SendBytes(mac string, msg []byte) error {

	if len(msg) > MTU {
		return fmt.Errorf("message of length %d is larger than MTU %d", len(msg), MTU)
	}

	mac_cstr := C.CString(mac)
	defer C.free(unsafe.Pointer(mac_cstr))

	C.send_msg(m.msg, mac_cstr, (*C.char)(unsafe.Pointer(&msg[0])), C.size_t(len(msg)))

	if C.error_msg(m.msg) != nil {
		return errors.New(C.GoString(C.error_msg(m.msg)))
	}

	runtime.KeepAlive(msg)

	return nil
}

func (m *Msg) Recv(buffer []byte) (int, error) {
	read := C.recv_msg(m.msg, unsafe.Pointer(&buffer[0]), C.size_t(len(buffer)))

	if C.error_msg(m.msg) != nil {
		return 0, errors.New(C.GoString(C.error_msg(m.msg)))
	}

	return int(read), nil
}

func F() {
	mac := "18:c0:4d:2f:16:46"
	// mac := "a4:7a:a4:f8:2b:94"

	var ether_type uint16 = 0x88b5
	m, err := NewMsg("en0", ether_type)
	defer m.Destroy()
	if err != nil {
		panic(err)
	}

	if err := m.SendString(mac, "hihihihihihihihihi"); err != nil {
		panic(err)
	}

	buffer := make([]byte, 100)
	if len, err := m.Recv(buffer); err != nil {
		println("RECV ", len)
	} else {
		println("RRRR ", len, string(buffer), "---")
	}

	// runtime.KeepAlive(m)
}
