package retry

import (
	"fmt"
	"time"
)

type ExponentialBackoff struct {
	Max          time.Duration
	Initial      time.Duration
	Current      time.Duration
	Count        int
	InitialCount int
}

func NewExponentialBackoff(count int, initial, max time.Duration) (*ExponentialBackoff, error) {

	bo := &ExponentialBackoff{
		Max:          max,
		Initial:      initial,
		Current:      initial,
		Count:        count,
		InitialCount: count,
	}

	if bo.Max < 0 {
		return nil, fmt.Errorf("max duration cannot be negative value %d", bo.Max)
	}
	if bo.Initial < 0 {
		return nil, fmt.Errorf("initial duration cannot be negative value %d", bo.Initial)
	}
	if bo.Count < 0 {
		return nil, fmt.Errorf("count may not be negative")
	}

	return bo, nil
}

func (bo *ExponentialBackoff) Init(r *Retry) (time.Duration, error) {
	bo.Current = bo.Initial
	bo.Count = bo.InitialCount
	return bo.Initial, nil
}

func (bo *ExponentialBackoff) Retry() (time.Duration, error) {

	if bo.Count == 0 {
		return 0, fmt.Errorf("retries exhausted")
	}

	bo.Count--

	if bo.Current < 2 {
		// If the duration is ever so small it *never* grows, make it 2.
		bo.Current = 2
	} else {
		bo.Current = bo.Current * bo.Current
	}

	if bo.Current > bo.Max {
		bo.Current = bo.Max
	}

	// FIXME - can go multiplication result in a negative number, ever?
	if bo.Current < 0 {
		bo.Current = bo.Max
	}

	return bo.Current, nil
}
