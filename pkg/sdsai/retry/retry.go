package retry

import (
	"fmt"
	"time"
)

type RetryPolicy interface {
	Init(*Retry) (time.Duration, error)
	Retry() (time.Duration, error)
}

type Retry struct {
	// The timer that triggers the retry event.
	timer *time.Timer
	retry RetryPolicy

	// When this Retry completes, either by success or by exhausting retries,
	// this is called.
	// On success, error is nil. On exhausted retries or an unexpected error
	// the error is passed to the function.
	CompletionHandler func(error)
}

func NewRetry(policy RetryPolicy) *Retry {
	return &Retry{
		timer: nil,
		retry: policy,
		CompletionHandler: func(e error) {
			if e != nil {
				fmt.Printf("ERROR: %s", e)
			}
		},
	}
}

func (r *Retry) Stop() {
	if r.timer != nil && !r.timer.Stop() {
		select {
		case <-r.timer.C:
		default:
		}
	}
}

func (r *Retry) Retry(userFunction func() error) error {
	// Be sure we are stopped.
	r.Stop()

	if d, err := r.retry.Init(r); err != nil {
		return err
	} else {

		f := func() {
			// Call the user's function.
			// If there is an error, the function has to be retried again.
			if err = userFunction(); err != nil {

				// If this executes, we *know* the timer has fired. We are that function.
				if d, err := r.retry.Retry(); err != nil {
					// Do not reschedule.
					r.CompletionHandler(err)
				} else {
					// Reset with the computed new duration.
					r.timer.Reset(d)
				}
			} else {
				// Success!
				r.CompletionHandler(nil)
			}

		}

		r.timer = time.AfterFunc(d, f)
	}

	return nil
}

func NewExponentialBackoffRetry(count int, initial, max time.Duration) (*Retry, error) {
	bo, err := NewExponentialBackoff(count, initial, max)
	if err != nil {
		return nil, err
	}

	return NewRetry(bo), nil
}
