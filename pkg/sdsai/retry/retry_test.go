package retry

import (
	"errors"
	"testing"
)

func TestRetry(t *testing.T) {
	p, err := NewExponentialBackoff(3, 1, 100)
	if err != nil {
		t.Fatalf("%s", err)
	}

	r := NewRetry(p)

	receiver := make(chan int)
	i := 1

	r.Retry(func() error {
		receiver <- i
		i += 1
		return errors.New("Fail")
	})

	i1 := <-receiver
	if i1 != 1 {
		t.Fatalf("expected 1 and got %d", i1)
	}

	i2 := <-receiver
	if i2 != 2 {
		t.Fatalf("expected 2 and got %d", i2)
	}

	i3 := <-receiver
	if i3 != 3 {
		t.Fatalf("expected 3 and got %d", i3)
	}

	i4 := <-receiver
	if i4 != 4 {
		t.Fatalf("expected 4 and got %d", i4)
	}

	r.Stop()
}
