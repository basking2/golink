package bytestream

import (
	"fmt"
)

func (bs *ByteStream) AddSegment(data []byte) error {

	if SeqNum(len(data)) > bs.SegmentSize {
		return fmt.Errorf("segment size of %d exceeds maximums size of %d", len(data), bs.SegmentSize)
	}

	if bs.Size+SeqNum(len(data)) > bs.WindowSize {
		return fmt.Errorf("adding %d bytes will exceed the window size of %d", len(data), bs.WindowSize)
	}

	bs.Segments.PushBack(NewUnackedByteStreamSegment(data))
	bs.Size += SeqNum(len(data))
	return nil
}
