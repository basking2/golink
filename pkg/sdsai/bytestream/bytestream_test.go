package bytestream

import (
	"runtime"
	"testing"

	"gitlab.com/basking2/golink/pkg/sdsai/sequence_numbers"
)

func TestAddSegment(t *testing.T) {
	bs, err := NewByteStream(1000, 100)
	if err != nil {
		t.Fatalf("Cannot construct a byte stream: %s", err)
	}

	_, err = bs.Write([]byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9})
	if err != nil {
		t.Fatalf("Cannot add segment: %s", err)
	}
}

func TestIncSequenceNumber(t *testing.T) {
	var i SeqNum = sequence_numbers.MaxSeqU64 - 10

	i = i.Inc(10)
	if i != sequence_numbers.MaxSeqU64 {
		t.Fatalf("Expected %d but was %d.", sequence_numbers.MaxSeqU64, i)
	}

	i = i.Inc(0)
	if i != sequence_numbers.MaxSeqU64 {
		t.Fatalf("Expected %d but was %d.", sequence_numbers.MaxSeqU64, i)
	}

	i = i.Inc(1)
	if i != 0 {
		t.Fatalf("Expected %d but was %d.", 0, i)
	}
}

func TestSequenceNumberDiff(t *testing.T) {
	if 20 != SeqNum(20).Diff(40) {
		t.Fatalf("Diff of 40-20 should be 20.")
	}

	var i1 SeqNum = sequence_numbers.MaxSeqU64 - 10
	var i2 SeqNum = i1.Inc(20)
	var diff SeqNum = i1.Diff(i2)
	if diff != 20 {
		t.Fatalf("Diff was %d instead of 20.", diff)
	}

	diff = i2.Diff(i1)
	if diff != sequence_numbers.MaxSeqU64-19 {
		t.Fatalf("Diff was %d instead of %d.", diff, sequence_numbers.MaxSeqU64-19)
	}

	for i, j1 := 0, sequence_numbers.MaxSeqU64-100; i < 100; i++ {
		j2 := j1.Inc(10)

		// Debug code for evaluating the test.
		// fmt.Printf("%d-%d=...\n", j2, j1)
		if 10 != j1.Diff(j2) {
			t.Fatalf("Expected diff to be 10. j1=%d j2=%d and diff %d", j1, j2, j1.Diff(j2))
		}
		j1 = j2
	}
}

func ack_test_fixture(t *testing.T) *ByteStream {
	bs, err := NewByteStream(10000, 100)

	if err != nil {
		t.Fatalf("Error %s", err)
	}

	bs.Write([]byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9})
	bs.Write([]byte{10, 11, 12, 13, 14, 15, 16, 17, 18, 19})
	bs.Write([]byte{20, 21, 22, 23, 24, 25, 26, 27, 28, 29})
	bs.Write([]byte{30, 31, 32, 33, 34, 35, 36, 37, 38, 39})
	bs.Write([]byte{40, 41, 42, 43, 44, 45, 46, 47, 48, 49})

	return bs
}

func expect(t *testing.T, what string, expected, actual any) {
	if expected != actual {

		_, file, line, _ := runtime.Caller(1)

		t.Fatalf("%s:%d expected %s is %s but got %s.", file, line, what, expected, actual)
	}
}

func TestAckBeforeSegment(t *testing.T) {
	bs := ack_test_fixture(t)

	expect(t, "size", SeqNum(50), bs.Size)
	expect(t, "seqnumber", SeqNum(0), bs.SequenceNumber)

	bs.Acknowledge(sequence_numbers.MaxSeqU64-10, SeqNum(10))
	expect(t, "after size", SeqNum(50), bs.Size)
	expect(t, "after seqnumber", SeqNum(0), bs.SequenceNumber)
}

func TestAckAfterSegment(t *testing.T) {
	bs := ack_test_fixture(t)

	expect(t, "size", SeqNum(50), bs.Size)
	expect(t, "segments", 5, bs.Segments.Len())
	expect(t, "seqnumber", SeqNum(0), bs.SequenceNumber)

	bs.Acknowledge(50, 60)
	expect(t, "after size", SeqNum(50), bs.Size)
	expect(t, "segments", 5, bs.Segments.Len())
	expect(t, "after seqnumber", SeqNum(0), bs.SequenceNumber)

	bs.Acknowledge(49, 60)
	expect(t, "after size", SeqNum(50), bs.Size)
	expect(t, "segments", 6, bs.Segments.Len())
	expect(t, "after seqnumber", SeqNum(0), bs.SequenceNumber)
}

func TestAckWithinSegment(t *testing.T) {
	bs := ack_test_fixture(t)

	expect(t, "size", SeqNum(50), bs.Size)
	expect(t, "segments", 5, bs.Segments.Len())
	expect(t, "seqnumber", SeqNum(0), bs.SequenceNumber)

	bs.Acknowledge(50, 60)
	expect(t, "after size", SeqNum(50), bs.Size)
	expect(t, "segments", 5, bs.Segments.Len())
	expect(t, "after seqnumber", SeqNum(0), bs.SequenceNumber)

	// Ack the very last byte.
	bs.Acknowledge(5, 1)
	expect(t, "after size", SeqNum(50), bs.Size)
	expect(t, "segments", 7, bs.Segments.Len())
	expect(t, "after seqnumber", SeqNum(0), bs.SequenceNumber)
}

func TestAckAboutSegment(t *testing.T) {
	bs := ack_test_fixture(t)

	expect(t, "size", SeqNum(50), bs.Size)
	expect(t, "segments", 5, bs.Segments.Len())
	expect(t, "seqnumber", SeqNum(0), bs.SequenceNumber)

	bs.Acknowledge(50, 60)
	expect(t, "after size", SeqNum(50), bs.Size)
	expect(t, "segments", 5, bs.Segments.Len())
	expect(t, "after seqnumber", SeqNum(0), bs.SequenceNumber)

	// Ack the very last byte.
	bs.Acknowledge(sequence_numbers.MaxSeqU64, 11)
	expect(t, "after size", SeqNum(40), bs.Size)
	expect(t, "segments", 4, bs.Segments.Len())
	expect(t, "after seqnumber", SeqNum(10), bs.SequenceNumber)
}

func TestAckFrontHalfSegment(t *testing.T) {
	bs := ack_test_fixture(t)

	expect(t, "size", SeqNum(50), bs.Size)
	expect(t, "segments", 5, bs.Segments.Len())
	expect(t, "seqnumber", SeqNum(0), bs.SequenceNumber)

	bs.Acknowledge(50, 60)
	expect(t, "after size", SeqNum(50), bs.Size)
	expect(t, "segments", 5, bs.Segments.Len())
	expect(t, "after seqnumber", SeqNum(0), bs.SequenceNumber)

	bs.Acknowledge(sequence_numbers.MaxSeqU64, 5)
	expect(t, "after size", SeqNum(46), bs.Size)
	expect(t, "segments", 5, bs.Segments.Len())
	expect(t, "after seqnumber", SeqNum(4), bs.SequenceNumber)

}

func TestAckBackHalfSegment(t *testing.T) {
	bs := ack_test_fixture(t)

	expect(t, "size", SeqNum(50), bs.Size)
	expect(t, "segments", 5, bs.Segments.Len())
	expect(t, "seqnumber", SeqNum(0), bs.SequenceNumber)

	bs.Acknowledge(50, 60)
	expect(t, "after size", SeqNum(50), bs.Size)
	expect(t, "segments", 5, bs.Segments.Len())
	expect(t, "after seqnumber", SeqNum(0), bs.SequenceNumber)

	// Ack the very last byte.
	bs.Acknowledge(49, 60)
	expect(t, "after size", SeqNum(50), bs.Size)
	expect(t, "segments", 6, bs.Segments.Len())
	expect(t, "after seqnumber", SeqNum(0), bs.SequenceNumber)
}

func TestAckConsecutiveSegments(t *testing.T) {
	bs := ack_test_fixture(t)

	expect(t, "size", SeqNum(50), bs.Size)
	expect(t, "segments", 5, bs.Segments.Len())
	expect(t, "seqnumber", SeqNum(0), bs.SequenceNumber)

	bs.Acknowledge(50, 60)
	expect(t, "after size", SeqNum(50), bs.Size)
	expect(t, "segments", 5, bs.Segments.Len())
	expect(t, "after seqnumber", SeqNum(0), bs.SequenceNumber)

	bs.Acknowledge(10, 60)
	expect(t, "after size", SeqNum(50), bs.Size)
	expect(t, "segments", 5, bs.Segments.Len())
	expect(t, "after seqnumber", SeqNum(0), bs.SequenceNumber)

	bs.Acknowledge(0, 10)
	expect(t, "after size", SeqNum(0), bs.Size)
	expect(t, "segments", 0, bs.Segments.Len())
	expect(t, "after seqnumber", SeqNum(50), bs.SequenceNumber)

}
