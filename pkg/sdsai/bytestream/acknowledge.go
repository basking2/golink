package bytestream

import (
	"container/list"
	"fmt"
)

// Acknowledge bytes. Acknowledged bytes are removed from this structure.
// This method is asynchronous. Acknowledgements happen in-order but
// may not be complete when this function returns.
func (bs *ByteStream) Acknowledge(start, length SeqNum) error {

	// The current sequence number. Recall that this can wrap back to 0.
	seqnum := bs.SequenceNumber
	element := bs.Segments.Front()

	// Walk the segment list.
	for keep_going := true; element != nil && keep_going; {
		switch element.Value.(type) {
		case *ByteStreamSegment:
			seg := element.Value.(*ByteStreamSegment)

			// DEBUG CODE
			// fmt.Print("---------------------------------------\n")
			// fmt.Printf("--sg pre -- > %s\n", seg)

			keep_going = bs.ack_or_split_segment(element, seg, seqnum, start, length)

			// DEBUG CODE
			// fmt.Printf("--sg post-- > %s\n", seg)
			// fmt.Printf("--bs ------ > %s\n", bs)

			if seg.Acknowledged && bs.is_first_segment(seg) {
				bs.SequenceNumber = bs.SequenceNumber.Inc(seg.Size)
				bs.Size -= seg.Size
				bs.Segments.Remove(element)
				element = bs.Segments.Front()
			}

			seqnum = seqnum.Inc(seg.Size)
			element = element.Next()
		default:
			panic(fmt.Errorf("unknown type in segment list: %s", element.Value))
		}
	}

	bs.drain_acked_segments()

	return nil
}

// Remove any segments that are in front of the list and are fully acked.
func (bs *ByteStream) drain_acked_segments() {
	element := bs.Segments.Front()

	for element != nil {
		switch element.Value.(type) {
		case *ByteStreamSegment:
			seg := element.Value.(*ByteStreamSegment)

			if !seg.Acknowledged {
				return
			}

			if !bs.is_first_segment(seg) {
				return
			}
			bs.SequenceNumber = bs.SequenceNumber.Inc(seg.Size)
			bs.Size -= seg.Size
			bs.Segments.Remove(element)
			element = bs.Segments.Front()
		default:
			panic(fmt.Errorf("unknown type in segment list: %s", element.Value))
		}
	}
}

func (bs *ByteStream) ack_or_split_segment(
	element *list.Element,
	seg *ByteStreamSegment,
	seqnum,
	start,
	length SeqNum) bool {

	if start.InRange(length, seqnum) {
		// Start falls in range.

		if seg.end_in_range(start, length, seqnum) {
			// Last sequence number falls in range.
			// FULLY encompassed segment. Simple. It's all gone.
			seg.Acknowledged = true
			seg.Data = nil
			return true

		} else if bs.is_first_segment(seg) {

			// Compute the bytes NOT acked.
			// That is, those bytes after the end of our sequence.
			unacked_bytes := start.Inc(length).Diff(seqnum.Inc(seg.Size))
			acked_bytes := seg.Size - unacked_bytes

			// Trim off the front.
			bs.SequenceNumber = bs.SequenceNumber.Inc(acked_bytes)
			seg.Data = seg.Data[acked_bytes:]
			seg.Size = unacked_bytes
			bs.Size -= acked_bytes

			// Signal to stop. The end is outside the acked range.
			return false
		} else {
			// Internal segment. Split this.
			// Compute the bytes NOT acked.
			// That is, those bytes after the end of our sequence.
			unacked_bytes := start.Inc(length).Diff(seqnum.Inc(seg.Size))
			acked_bytes := seg.Size - unacked_bytes

			// Create a segment representing the acked bytes.
			acked_segment := NewAckedByteStreamSegment(acked_bytes)

			// Shrink this segment down to contain only the unacked bytes.
			seg.Size = unacked_bytes
			seg.Data = seg.Data[acked_bytes:]

			// Insert the acked segment before this one.
			bs.Segments.InsertBefore(acked_segment, element)

			// Signal to stop. The end is outside the acked range.
			return false
		}
	} else if seg.end_in_range(start, length, seqnum) {
		// Segment start is before the ack start, so seqnum is before start.

		unacked_bytes := seqnum.Diff(start)
		acked_bytes := seg.Size - unacked_bytes

		// Bytes before are not acked.
		unacked_segment := NewUnackedByteStreamSegment(seg.Data[0:unacked_bytes])

		// Update this segment to be smaller and to be acked.
		seg.Size = acked_bytes
		seg.Acknowledged = true
		seg.Data = seg.Data[unacked_bytes:]

		bs.Segments.InsertBefore(unacked_segment, element)

		// The acked range is beyond the end of this segment. Signal to continue acking.
		return true
	} else if seqnum.InRange(seg.Size, start) {
		// That strange strange case where the data segment fully encloses the acked portion. Odd, but we'll cover it.
		// Split into unacked, acked, unacked segments.

		// 0 is the start of the unacked portion.
		ack_start := seqnum.Diff(start)
		ack_end := ack_start + length

		// Make two preceding nodes and update our existing node's size and data.
		unacked_segment := NewUnackedByteStreamSegment(seg.Data[0:ack_start])
		acked_segment := NewAckedByteStreamSegment(length)
		seg.Data = seg.Data[ack_end:]
		seg.Size = seg.Size - ack_end

		bs.Segments.InsertBefore(unacked_segment, element)
		bs.Segments.InsertBefore(acked_segment, element)

		// Signal to stop. The end is outside the acked range.
		return false
	}

	return true
}

func (seg *ByteStreamSegment) end_in_range(start, length, seqnum SeqNum) bool {

	// fmt.Printf("end_in_range: %d + %d where is %d\n", start, length, inc_seq_num(seqnum, seg.Size-1))

	return start.InRange(length, seqnum.Inc(seg.Size-1))
}

func (bs *ByteStream) is_first_segment(seg *ByteStreamSegment) bool {
	return bs.Segments.Front().Value == seg
}
