package bytestream

import (
	"testing"
)

func TestWalkSegments(t *testing.T) {
	bs, err := NewByteStream(10000, 100)

	if err != nil {
		t.Fatalf("Error %s", err)
	}

	bs.Write([]byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9})
	bs.Write([]byte{10, 11, 12, 13, 14, 15, 16, 17, 18, 19})
	bs.Write([]byte{20, 21, 22, 23, 24, 25, 26, 27, 28, 29})
	bs.Write([]byte{30, 31, 32, 33, 34, 35, 36, 37, 38, 39})
	bs.Write([]byte{40, 41, 42, 43, 44, 45, 46, 47, 48, 49})

	i := 0

	bsArray := make([]*ByteStreamSegment, 0, 10)

	bs.WalkSegments(func(bs *ByteStream, seg *ByteStreamSegment) bool {
		bsArray = append(bsArray, seg)

		i += 1
		return i != 3
	})

	expect(t, "cap", 10, cap(bsArray))
	expect(t, "len", 3, len(bsArray))
	expect(t, "len", SeqNum(10), bsArray[0].Size)
	expect(t, "len", SeqNum(10), bsArray[1].Size)
	expect(t, "len", SeqNum(10), bsArray[2].Size)

}
