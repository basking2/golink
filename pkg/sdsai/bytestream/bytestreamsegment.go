package bytestream

type ByteStreamSegment struct {
	// The data to send or, in the case of Acknowledged data, a 0-length slice.
	Data []byte

	// The length of Data.
	Size SeqNum

	// True if the data has been sent and acknowledged.
	// When this is true Data is set to nil but Size remains to compute
	// Correct offsets from the ByteStream SequenceNumber.
	Acknowledged bool
}

// Create a byte stream segment representing a acked bytes.
// Acked bytes are not stored.
func NewAckedByteStreamSegment(size SeqNum) *ByteStreamSegment {
	return &ByteStreamSegment{nil, size, true}
}

// Create a byte stream segment representing a unacked bytes.
func NewUnackedByteStreamSegment(data []byte) *ByteStreamSegment {
	return &ByteStreamSegment{data, SeqNum(len(data)), false}
}
