package bytestream

import (
	"container/list"

	"gitlab.com/basking2/golink/pkg/sdsai/sequence_numbers"
)

type SeqNum = sequence_numbers.SeqNumU64

// This represents a stream of bytes.
// The SequenceNumber represents the number of the first byte stored.
// Subsequent sequence numbers are computed off this initial sequence number.
type ByteStream struct {
	// The maximum amount of data that this structure will store.
	WindowSize SeqNum

	// The maximum size of a segment this structure will store.
	SegmentSize SeqNum

	// Current total size of stored data.
	Size SeqNum

	// The number of the first byte in this structure.
	SequenceNumber SeqNum

	// The list of segment pointers.
	Segments *list.List
}

func NewByteStream(windowSize, segmentSize SeqNum) (*ByteStream, error) {
	bs := ByteStream{
		windowSize, segmentSize, 0, 0, list.New(),
	}

	return &bs, nil
}

func (bs *ByteStream) Write(data []byte) (int, error) {
	err := bs.AddSegment(data)

	if err == nil {
		return len(data), nil
	} else {
		return 0, err
	}
}
