package bytestream

import "fmt"

func (bs *ByteStream) String() string {
	return fmt.Sprintf("ByteStream segsize=%d, winsz=%d, segments=%d, seqnum=%d, sz=%d",
		bs.SegmentSize,
		bs.WindowSize,
		bs.Segments.Len(),
		bs.SequenceNumber,
		bs.Size)
}

func (seg *ByteStreamSegment) String() string {
	return fmt.Sprintf("ByteStreamSegment segsize=%d, acknolwedged=%t",
		seg.Size,
		seg.Acknowledged)
}
