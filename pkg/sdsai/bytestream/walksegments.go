package bytestream

// Walk the byte segments, calling the handler on each one.
//
// Tracking the sequence number must be done by the callback, if desired.
//
// If the handler returns true, iteration continues and, eventually this
// function will return true, indicating the iteration was never stopped by
// the handler.
//
// If the handler return false, then iteration does not continue and
// this function returns true.
func (bs *ByteStream) WalkSegments(handler func(*ByteStream, *ByteStreamSegment) bool) bool {
	for element := bs.Segments.Front(); element != nil; element = element.Next() {
		if !handler(bs, element.Value.(*ByteStreamSegment)) {
			return false
		}
	}

	return true
}
