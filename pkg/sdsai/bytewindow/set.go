package bytewindow

import (
	"fmt"
)

func (bw *ByteWindow) contains_sequence_number(seqnum SeqNum) bool {
	return bw.SequenceNumber.InRange(bw.Size, seqnum)
}

// Given the starting sequence number of this segment and a target number to question about,
// return true if the target sequence number appears in this segment.
func (seg *ByteWindowSegment) contains_sequence_number(start, target SeqNum) bool {
	return start.InRange(seg.Size, target)
}

func (bw *ByteWindow) simple_append_set(data []byte, seqnum SeqNum) error {
	// Simple append
	if SeqNum(len(data)+int(bw.Size)) > bw.WindowSize {
		return fmt.Errorf("adding this segment exceeds our allowed size window of %d bytes", bw.WindowSize)
	}

	bw.Segments.PushBack(
		&ByteWindowSegment{
			Data:  data,
			Size:  SeqNum(len(data)),
			IsGap: false,
		})

	bw.Size += SeqNum(len(data))

	return nil
}

// Split the given byte window segment by editing it and returning the preceding segment.
// If this is a gap segment, the data is null.
// If this is a data segment, the data is divided correctly.
func (seg *ByteWindowSegment) split(length SeqNum) *ByteWindowSegment {

	if seg.IsGap {
		newseg := ByteWindowSegment{IsGap: true, Size: length, Data: nil}

		seg.Size -= length

		return &newseg
	} else {
		newseg := ByteWindowSegment{IsGap: false, Size: length, Data: seg.Data[:length]}
		seg.Size -= length
		seg.Data = seg.Data[length:]
		return &newseg
	}
}

func last_sequence_number(data []byte, seqnum SeqNum) SeqNum {
	return seqnum.Inc(SeqNum(len(data)))
}

func (bw *ByteWindow) window_contains_seqnum(seqnum SeqNum) bool {
	return bw.SequenceNumber.InRange(bw.WindowSize, seqnum)
}

// Set the given bytes starting at the given sequence number.
func (bw *ByteWindow) Set(data []byte, data_seqnum SeqNum) error {

	if bw.SequenceNumber.Inc(bw.Size) == data_seqnum {
		return bw.simple_append_set(data, data_seqnum)

	} else if bw.window_contains_seqnum(data_seqnum) {

		// Make sure this data segment fit in the window.
		if !bw.window_contains_seqnum(last_sequence_number(data, data_seqnum)) {
			return fmt.Errorf("data segment %d length %d would extend the window beyond its allowable size", data_seqnum, len(data))
		}

		// If this starts in the already existing range.
		// This means that we will be replacing gap segments with data
		// or we've already received this data and will be skipping the data.
		//
		// In either case, find the segment where this data starts and begin
		// filling.

		seg_seqnum := bw.SequenceNumber
		for e := bw.Segments.Front(); e != nil; e = e.Next() {
			seg := e.Value.(*ByteWindowSegment)

			// If the segment's sequence number is equal to the start of data...
			if seg_seqnum == data_seqnum {

				// ... and if the segments are the same size...
				if seg.Size == SeqNum(len(data)) {
					// ... and the segment is a gap, just fill in the data. Done.
					if seg.IsGap {
						seg.IsGap = false
						seg.Data = data
					}

					return nil

					// If the segment is smaller, this data extends beyond it.
				} else if seg.Size < SeqNum(len(data)) {

					// If this segment is a gap, fill it with data.
					if seg.IsGap {
						seg.Data = data[:seg.Size]
						seg.IsGap = false
					}

					// Shrink the data we must consider and let the loop continue.
					data = data[seg.Size:]
					data_seqnum = data_seqnum.Inc(seg.Size)

					// If the segment is larger than this data.
				} else {

					// If the segment is a gap, insert a data segment
					// before this segment and reduce the size of this
					// segment. We are then done.
					if seg.IsGap {
						// ... insert a segment before this one with data.
						newseg := ByteWindowSegment{
							Data:  data,
							Size:  SeqNum(len(data)),
							IsGap: false,
						}
						bw.Segments.InsertBefore(&newseg, e)

						// edit this segment...
						seg.Size -= newseg.Size
					}

					return nil
				}

				// If the data does not start this segment, but does appear in the segment.
			} else if seg.contains_sequence_number(seg_seqnum, data_seqnum) {
				// NOT the starting sequence, but in the segment.

				// How much data is covered by this segment, ignoring the start.
				seg_size_left := seg.Size - seg_seqnum.Diff(data_seqnum)

				// Data fits in this segment. There is no further iteration.
				if seg_size_left == SeqNum(len(data)) {

					if seg.IsGap {
						// Split this gap segment and insert the gap before us.
						leading_gap_seg := seg.split(seg.Size - seg_size_left)
						bw.Segments.InsertBefore(leading_gap_seg, e)
						seg.Data = data
						seg.IsGap = false
					}

					return nil

					// The segment fits inside this segment.
					// Split this segment twice.
					// First, with a preceding gap.
					// Then with a preceding data segment.
					// Finally, this segment is left as a gap segment.
				} else if seg_size_left > SeqNum(len(data)) {

					// If this is a gap segment, split it.
					// If it is not a gap segment, we are done and return nil.
					if seg.IsGap {
						// Split this gap segment and insert the gap before us.
						leading_gap_seg := seg.split(seg.Size - seg_size_left)
						bw.Segments.InsertBefore(leading_gap_seg, e)

						data_seg := seg.split(SeqNum(len(data)))
						data_seg.IsGap = false
						data_seg.Data = data
						bw.Segments.InsertBefore(data, e)
					}

					return nil

					// Data extends beyond this segment and this segment is a gap.
				} else {
					if seg.IsGap {
						// Create a segment to hold the new gap.
						newseg := seg.split(seg.Size - seg_size_left)
						bw.Segments.InsertBefore(newseg, e)

						// Adjust the sequence number of the current segment.
						seg_seqnum = seg_seqnum.Inc(newseg.Size)

						// Now fill the current segment with data.
						seg.IsGap = false
						seg.Size = seg_size_left
						seg.Data = data[0:seg_size_left]

						// Next iteration, we pull the next list element with a smaller data segment.
					}

					data = data[seg_size_left:]
					data_seqnum = data_seqnum.Inc(seg_size_left)
				}
			}

			// Compute sequence number of the next segment.
			seg_seqnum = seg_seqnum.Inc(seg.Size)
		}

		// If we reach here without exiting, we know...
		// 1. The data is to be inserted at the end of this sequence.
		// 2. There is enough room in the window to append this data.
		// 3. The seg_seqnum is pointing to the next sequence number to be added.

		if seg_seqnum != data_seqnum {
			// We must append a gap segment.
			empty_segment_size := seg_seqnum.Diff(data_seqnum)

			empty_segment := ByteWindowSegment{
				Data:  nil,
				Size:  empty_segment_size,
				IsGap: true,
			}

			bw.Segments.PushBack(&empty_segment)
			bw.Size += empty_segment_size
		}

		data_seg := ByteWindowSegment{IsGap: false, Size: SeqNum(len(data)), Data: data}
		bw.Segments.PushBack(&data_seg)
		bw.Size += data_seg.Size

		return nil
	} else {
		// If we get here, the data is not in the existing range and not in the allowable size.
		return fmt.Errorf("bytes do not appear in the existing/allowed range")
	}
}
