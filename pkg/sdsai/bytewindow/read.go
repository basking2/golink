package bytewindow

import (
	"fmt"
)

// Read all available bytes from the given ByteWindow into the buffer.
// This will advance the sequence number of the window and remove data.
// Reading stops when the first gap segment is encountered or all data is
// consumed.
func (bw *ByteWindow) Read(data []byte) (int, error) {

	total_bytes_read := 0

	for e := bw.Segments.Front(); e != nil; {

		seg := e.Value.(*ByteWindowSegment)

		// Stop at the first gap.
		if seg.IsGap {
			return total_bytes_read, nil
		}

		// Partial read. We are done after this read.
		if seg.Size+SeqNum(total_bytes_read) > SeqNum(len(data)) {
			bytes_read := copy(data[total_bytes_read:], seg.Data)
			total_bytes_read += bytes_read

			// Shrink this segment by the bytes read.
			seg.Data = seg.Data[bytes_read:]
			seg.Size -= SeqNum(bytes_read)

			// Advance the window's sequence number.
			bw.SequenceNumber = bw.SequenceNumber.Inc(SeqNum(total_bytes_read))

			break
		} else {

			// Else, full read!
			bytes_read := copy(data[total_bytes_read:], seg.Data)
			total_bytes_read += bytes_read

			// Error checking. This should never happen.
			if bytes_read != int(seg.Size) {
				panic(fmt.Sprintf("read %d bytes when %d are available from %+v", bytes_read, seg.Size, seg))
			}

			remove_me := e
			e = e.Next()
			bw.Segments.Remove(remove_me)
		}
	}

	bw.SequenceNumber = bw.SequenceNumber.Inc(SeqNum(total_bytes_read))

	// All elements are consumed.
	return total_bytes_read, nil
}
