package bytewindow

func (bw *ByteWindow) Write(data []byte) (int, error) {
	if err := bw.Set(data, bw.SequenceNumber+bw.Size); err != nil {
		return 0, err
	}

	return len(data), nil
}
