package bytewindow

import (
	"bytes"
	"fmt"
	"runtime"
	"testing"
)

func TestSetAfterStart(t *testing.T) {
	bw, err := NewByteWindow(1000, 10)

	if err != nil {
		t.Fatalf("failed %s", err)
	}

	// Set 20-30.
	err = bw.Set([]byte{2, 2, 2, 2, 2, 2, 2, 2, 2, 2}, 20)
	if err != nil {
		t.Fatalf("err: %s", err)
	}

	e := bw.Segments.Front()

	seg := e.Value.(*ByteWindowSegment)

	// 10 - 19
	checkSeg(t, seg, true, 10, nil)

	// 20 - 29
	e = e.Next()
	seg = e.Value.(*ByteWindowSegment)
	checkSeg(t, seg, false, 10, []byte{2, 2, 2, 2, 2, 2, 2, 2, 2, 2})

}

func TestSet(t *testing.T) {
	bw, err := NewByteWindow(1000, 10)

	if err != nil {
		t.Fatalf("failed %s", err)
	}

	// Append from start.
	err = bw.Set([]byte{1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, 0)
	if fmt.Sprintf("%s", err) != "bytes do not appear in the existing/allowed range" {
		t.Fatalf("got %s", err)
	}

	// Set 10-20.
	err = bw.Set([]byte{2, 2, 2, 2, 2, 2, 2, 2, 2, 2}, 10)
	if err != nil {
		t.Fatalf("err: %s", err)
	}

	// Set 30-39.
	err = bw.Set([]byte{3, 3, 3, 3, 3, 3, 3, 3, 3, 3}, 30)
	if err != nil {
		t.Fatalf("err: %s", err)
	}

	// Set 25-34
	err = bw.Set([]byte{4, 4, 4, 4, 4, 4, 4, 4, 4, 4}, 25)
	if err != nil {
		t.Fatalf("err: %s", err)
	}

	// Set 35-44
	err = bw.Set([]byte{5, 5, 5, 5, 5, 5, 5, 5, 5, 5}, 35)
	if err != nil {
		t.Fatalf("err: %s", err)
	}

	// Set 50-59
	err = bw.Set([]byte{6, 6, 6, 6, 6, 6, 6, 6, 6, 6}, 50)
	if err != nil {
		t.Fatalf("err: %s", err)
	}

	// Set 49-60
	err = bw.Set([]byte{7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7}, 49)
	if err != nil {
		t.Fatalf("err: %s", err)
	}

	e := bw.Segments.Front()

	seg := e.Value.(*ByteWindowSegment)

	// 10 - 19
	checkSeg(t, seg, false, 10, []byte{2, 2, 2, 2, 2, 2, 2, 2, 2, 2})

	// 20 - 24
	e = e.Next()
	seg = e.Value.(*ByteWindowSegment)
	checkSeg(t, seg, true, 5, nil)

	// 25 - 29
	e = e.Next()
	seg = e.Value.(*ByteWindowSegment)
	checkSeg(t, seg, false, 5, []byte{4, 4, 4, 4, 4})

	// 30 - 39
	e = e.Next()
	seg = e.Value.(*ByteWindowSegment)
	checkSeg(t, seg, false, 10, []byte{3, 3, 3, 3, 3, 3, 3, 3, 3, 3})

	// 40 - 44
	e = e.Next()
	seg = e.Value.(*ByteWindowSegment)
	checkSeg(t, seg, false, 5, []byte{5, 5, 5, 5, 5})

	// 45 - 48
	e = e.Next()
	seg = e.Value.(*ByteWindowSegment)
	checkSeg(t, seg, true, 4, nil)

	// 49
	e = e.Next()
	seg = e.Value.(*ByteWindowSegment)
	checkSeg(t, seg, false, 1, []byte{7})

	// 50 - 59
	e = e.Next()
	seg = e.Value.(*ByteWindowSegment)
	checkSeg(t, seg, false, 10, []byte{6, 6, 6, 6, 6, 6, 6, 6, 6, 6})

	// 60
	e = e.Next()
	seg = e.Value.(*ByteWindowSegment)
	checkSeg(t, seg, false, 1, []byte{7})
}

func checkSeg(t *testing.T, seg *ByteWindowSegment, isGap bool, size SeqNum, data []byte) {

	_, file, line, _ := runtime.Caller(1)

	// fmt.Printf("--> %+v\n", seg)

	if (isGap && !seg.IsGap) || (!isGap && seg.IsGap) {
		t.Fatalf("%s:%d gap expected to be %t in %+v", file, line, isGap, seg)
	}

	if seg.Size != size {
		t.Fatalf("%s:%d size expected to be %d in %+v", file, line, size, seg)
	}

	if !bytes.Equal(seg.Data, data) {
		t.Fatalf("%s:%d data expected to be %v in %+v", file, line, data, seg)
	}
}
