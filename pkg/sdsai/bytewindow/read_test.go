package bytewindow

import (
	"bytes"
	"testing"
)

func TestRead(t *testing.T) {
	bw, err := NewByteWindow(1000, 10)

	if err != nil {
		t.Fatalf("err %s", err)
	}

	if n, err := bw.Write([]byte{1, 2, 3, 4}); err != nil {
		t.Fatalf("err %s", err)
	} else if n != 4 {
		t.Fatalf("wrote only %d bytes when 4 were expected", n)
	}

	if n, err := bw.Write([]byte{5, 6, 7, 8}); err != nil {
		t.Fatalf("err %s", err)
	} else if n != 4 {
		t.Fatalf("wrote only %d bytes when 4 were expected", n)
	}

	err = bw.Set([]byte{9, 9, 9, 9}, bw.SequenceNumber+20)
	if err != nil {
		t.Fatalf("err %s", err)
	}

	buffer := make([]byte, 7)
	if n, err := bw.Read(buffer); err != nil {
		t.Fatalf("err %s", err)
	} else if n != 7 {
		t.Fatalf("read only %d bytes when 7 were expected", n)
	} else if !bytes.Equal(buffer, []byte{1, 2, 3, 4, 5, 6, 7}) {
		t.Fatalf("buffer has unexpected contents %+v", buffer)
	}

	if n, err := bw.Read(buffer); err != nil {
		t.Fatalf("err %s", err)
	} else if n != 1 {
		t.Fatalf("read only %d bytes when 1 was expected", n)
	} else if !bytes.Equal(buffer, []byte{8, 2, 3, 4, 5, 6, 7}) {
		t.Fatalf("buffer has unexpected contents %+v", buffer)
	}

}
