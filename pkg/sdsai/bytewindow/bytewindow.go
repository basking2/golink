package bytewindow

import (
	"container/list"

	"gitlab.com/basking2/golink/pkg/sdsai/sequence_numbers"
)

type SeqNum = sequence_numbers.SeqNumU64

type ByteWindowSegment struct {
	Data  []byte
	Size  SeqNum
	IsGap bool
}

// A byte window collects bytes in a window, tracking gaps, and allows reading from them.
type ByteWindow struct {
	// The maximum amount of data that this structure will store.
	WindowSize SeqNum

	// Current total size of stored data.
	Size SeqNum

	// The number of the first byte in this structure.
	SequenceNumber SeqNum

	// The list of segment pointers.
	Segments *list.List
}

func NewByteWindow(windowSize, seqnum SeqNum) (*ByteWindow, error) {
	bs := ByteWindow{
		windowSize, 0, seqnum, list.New(),
	}

	return &bs, nil
}
