package sequence_numbers

import (
	"math"
	"testing"
)

func TestIncSequenceNumber64(t *testing.T) {
	var i SeqNum64 = MaxSeq64 - 10

	i = i.Inc(10)
	if i != MaxSeq64 {
		t.Fatalf("Expected %d but was %d.", math.MaxInt64, i)
	}

	i = i.Inc(0)
	if i != math.MaxInt64 {
		t.Fatalf("Expected %d but was %d.", math.MaxInt64, i)
	}

	i = i.Inc(1)
	if i != 0 {
		t.Fatalf("Expected %d but was %d.", 0, i)
	}
}

func TestSequenceNumberDiff64(t *testing.T) {
	if 20 != SeqNum64(20).Diff(40) {
		t.Fatalf("Diff of 40-20 should be 20.")
	}

	var i1 SeqNum64 = MaxSeq64 - 10
	var i2 SeqNum64 = i1.Inc(20)
	var diff SeqNum64 = i1.Diff(i2)
	if diff != 20 {
		t.Fatalf("Diff was %d instead of 20.", diff)
	}

	diff = i2.Diff(i1)
	if diff != math.MaxInt-19 {
		t.Fatalf("Diff was %d instead of %d.", diff, math.MaxInt-19)
	}

	for i, j1 := 0, MaxSeq64-100; i < 100; i++ {
		j2 := j1.Inc(10)

		// Debug code for evaluating the test.
		// fmt.Printf("%d-%d=...\n", j2, j1)
		if 10 != j1.Diff(j2) {
			t.Fatalf("Expected diff to be 10. j1=%d j2=%d and diff %d", j1, j2, j1.Diff(j2))
		}
		j1 = j2
	}
}
