package sequence_numbers

import "math"

type SeqNumU64 uint64

var MaxSeqU64 SeqNumU64 = math.MaxUint64

// Increment n by d, wrapping to 0 if we exceed the max.
func (n SeqNumU64) Inc(d SeqNumU64) SeqNumU64 {
	return Inc(n, d, MaxSeqU64)
}

// Does the given sequence number fall in the range of seq and seq+len
// taking into account wrapping of values?
//
// Seq is the starting value and last value is computed from length.
func (seq SeqNumU64) InRange(length, num SeqNumU64) bool {
	return InRange(seq, length, num, MaxSeqU64)
}

// Find the difference between the two sequence numbers assuming the first
// one occurs first. Recall that sequence number wrap.
//
// Sequence numbers also never differ by ranges greater than the max value.
// That is, they do not wrap around their field of values.
func (lower SeqNumU64) Diff(higher SeqNumU64) SeqNumU64 {
	return Diff(lower, higher, MaxSeqU64)
}
