package sequence_numbers

import "math"

type SeqNumU32 uint32

var MaxSeqU32 SeqNumU32 = math.MaxUint32

// Increment n by d, wrapping to 0 if we exceed the max.
func (n SeqNumU32) Inc(d SeqNumU32) SeqNumU32 {
	return Inc(n, d, MaxSeqU32)
}

// Does the given sequence number fall in the range of seq and seq+len
// taking into account wrapping of values?
//
// Seq is the starting value and last value is computed from length.
func (seq SeqNumU32) InRange(length, num SeqNumU32) bool {
	return InRange(seq, length, num, MaxSeqU32)
}

// Find the difference between the two sequence numbers assuming the first
// one occurs first. Recall that sequence number wrap.
//
// Sequence numbers also never differ by ranges greater than the max value.
// That is, they do not wrap around their field of values.
func (lower SeqNumU32) Diff(higher SeqNumU32) SeqNumU32 {
	return Diff(lower, higher, MaxSeqU32)
}
