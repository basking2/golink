package sequence_numbers

import (
	"math"
	"testing"
)

func TestIncSequenceNumberU32(t *testing.T) {
	var i SeqNumU32 = MaxSeqU32 - 10

	i = i.Inc(10)
	if i != MaxSeqU32 {
		t.Fatalf("Expected %d but was %d.", math.MaxInt64, i)
	}

	i = i.Inc(0)
	if i != math.MaxUint32 {
		t.Fatalf("Expected %d but was %d.", math.MaxInt64, i)
	}

	i = i.Inc(1)
	if i != 0 {
		t.Fatalf("Expected %d but was %d.", 0, i)
	}
}

func TestSequenceNumberDiffU32(t *testing.T) {
	if 20 != SeqNumU32(20).Diff(40) {
		t.Fatalf("Diff of 40-20 should be 20.")
	}

	var i1 SeqNumU32 = MaxSeqU32 - 10
	var i2 SeqNumU32 = i1.Inc(20)
	var diff SeqNumU32 = i1.Diff(i2)
	if diff != 20 {
		t.Fatalf("Diff was %d instead of 20.", diff)
	}

	diff = i2.Diff(i1)
	if diff != math.MaxUint32-19 {
		t.Fatalf("Diff was %d instead of %d.", diff, math.MaxInt-19)
	}

	for i, j1 := 0, MaxSeqU32-100; i < 100; i++ {
		j2 := j1.Inc(10)

		// Debug code for evaluating the test.
		// fmt.Printf("%d-%d=...\n", j2, j1)
		if 10 != j1.Diff(j2) {
			t.Fatalf("Expected diff to be 10. j1=%d j2=%d and diff %d", j1, j2, j1.Diff(j2))
		}
		j1 = j2
	}
}
