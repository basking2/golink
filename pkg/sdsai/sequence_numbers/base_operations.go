package sequence_numbers

import "fmt"

type SequenceNumber interface {
	byte | int | uint | int16 | uint16 | int32 | uint32 | int64 | uint64 | SeqNumU32 | SeqNum64 | SeqNumU64
}

func Inc[NUM SequenceNumber](n, d, max NUM) NUM {
	if d < 0 {
		panic(fmt.Errorf("cannot increment %d with a negative value %d", n, d))
	}

	if n < 0 {
		panic(fmt.Errorf("cannot increment a negative value %d by %d", n, d))
	}

	// How much space is left if we remove d?
	n2 := max - n

	// If there is enough space to include n, just sum n and d.
	if n2 >= d {
		return n + d
	} else {
		// NOTE: Subtract 1 because we loop back to the value 0, not 1.
		return d - n2 - 1
	}

}

func InRange[NUM SequenceNumber](seq, length, num, max NUM) bool {
	start := seq
	end := Inc(seq, length, max)

	// Normal comparison.
	if start <= end {
		return start <= num && num < end
	}

	// Wrapped comparison from start through MaxInt64.
	if start <= num && num <= max {
		return true
	}

	// Wrapped comparison from 0 to end.
	if 0 <= num && num < end {
		return true
	}

	return false
}

func Diff[NUM SequenceNumber](lower, higher, max NUM) NUM {
	if lower < higher {
		// Simple case.
		return higher - lower
	}

	top_half := max - lower
	return higher + top_half + 1
}
