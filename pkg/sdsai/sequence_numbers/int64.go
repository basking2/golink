package sequence_numbers

import "math"

type SeqNum64 int64

var MaxSeq64 SeqNum64 = math.MaxInt64

// Increment n by d, wrapping to 0 if we exceed the max.
func (n SeqNum64) Inc(d SeqNum64) SeqNum64 {
	return Inc(n, d, MaxSeq64)
}

// Does the given sequence number fall in the range of seq and seq+len
// taking into account wrapping of values?
//
// Seq is the starting value and last value is computed from length.
func (seq SeqNum64) InRange(length, num SeqNum64) bool {
	return InRange(seq, length, num, MaxSeq64)
}

// Find the difference between the two sequence numbers assuming the first
// one occurs first. Recall that sequence number wrap.
//
// Sequence numbers also never differ by ranges greater than the max value.
// That is, they do not wrap around their field of values.
func (lower SeqNum64) Diff(higher SeqNum64) SeqNum64 {
	return Diff(lower, higher, MaxSeq64)
}
