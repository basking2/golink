all:
	# go  build -buildvcs=false ./...
	go build ./...
	go build ./cmd/golink

test:
	go test -timeout 10s -v ./...

fmt:
	go fmt ./...

run: all
	sudo ./golink

tcpdump:
	sudo tcpdump -i enp7s0 'ether proto 0x88b5'

sbeacons:
	sudo ./golink -i enp7s0 -e 18:c0:4d:2f:16:46 -m sbeacons

sbeaconr:
	sudo ./golink -i enp7s0 -e 18:c0:4d:2f:16:46 -m sbeaconr

main:
	cd pkg/golink/net && \
	g++ -D_BUILD_MAIN -o main *.c *.cpp
	
