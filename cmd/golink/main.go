package main

import (
	"flag"
	"fmt"
	"time"

	"gitlab.com/basking2/golink/pkg/golink/connection"
	"gitlab.com/basking2/golink/pkg/golink/dgram"
)

type config struct {
	Interface string
	MAC       string
	Mode      string
	EtherType int
	SrcPort   int
	DstPort   int
}

func main() {
	cfg := config{}
	flag.StringVar(&cfg.Interface, "i", "en0", "Specify the interface to bind.")
	flag.StringVar(&cfg.MAC, "e", "34:d0:b8:c0:aa:3d", "Specify the destination Ethernet MAC.")
	flag.StringVar(&cfg.Mode, "m", "recv", "Specify the mode. send, recv, beacon, sbeacons, sbeaconr.")
	flag.IntVar(&cfg.DstPort, "dstport", 2000, "Destination port for streaming operations.")
	flag.IntVar(&cfg.SrcPort, "srcport", 2000, "Source port for streaming operations.")
	flag.IntVar(&cfg.EtherType, "t", 0x88b5, "Ether type.")
	flag.Parse()

	switch cfg.Mode {
	case "send":
		dgs, err := dgram.NewDatagramServer(cfg.Interface)
		if err != nil {
			panic(err)
		}

		err = dgs.SendBytes(cfg.MAC, 1, []byte("Hello, world."))
		if err != nil {
			panic(err)
		}
	case "recv":
		dgs, err := dgram.NewDatagramServer(cfg.Interface)
		if err != nil {
			panic(err)
		}

		dg, err := dgs.RecvBytes()
		if err != nil {
			panic(err)
		}

		println(fmt.Sprintf("GOT %s", string(dg.Data)))
	case "beacon":
		dgs, err := dgram.NewDatagramServer(cfg.Interface)
		if err != nil {
			panic(err)
		}

		d, _ := time.ParseDuration("1s")

		for i := 0; true; i++ {
			err = dgs.SendBytes(cfg.MAC, 1, []byte(fmt.Sprintf("Message %d.", i)))
			if err != nil {
				panic(err)
			}

			time.Sleep(d)
		}
	case "sbeacons":
		connector, err := connection.NewConnector(uint16(cfg.EtherType), cfg.Interface, cfg.MAC)
		if err != nil {
			panic(err)
		}

		c := connector.NewConnection(connection.STREAM, connection.PORT(cfg.SrcPort), connection.PORT(cfg.DstPort))

		for {
			println("Pinging.")
			d, _ := time.ParseDuration("1s")
			_, err = c.WriteSync([]byte("ping"))
			if err != nil {
				panic(err)
			}
			<-time.NewTimer(d).C
		}

	}

}
